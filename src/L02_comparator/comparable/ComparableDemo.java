package L02_comparator.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparableDemo {
    public static void main(String[] args) {

        List<Employee> employeeList = new ArrayList<>();

        Employee employee1 = new Employee(10, "Andrey", 55.1);
        Employee employee2 = new Employee(1, "Ivan", 0.1);
        Employee employee3 = new Employee(20, "Natasha", 99.1);

        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);

        System.out.println(employeeList);
        Collections.sort(employeeList);
        System.out.println(employeeList);
    }
}
