package L07_stream;

import java.util.ArrayList;
import java.util.List;

public class DemoReduce {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();

        list.add(5);
        list.add(10);
        list.add(11);
        list.add(2);
        list.add(-6);
        list.add(1);

        Integer integer = list.stream()
                .reduce((accumulator, element) -> accumulator * element)
                .get();


        //первый параметр станет accumulator
        Integer integer1 = list.stream()
                .reduce(-2, (accumulator, element) -> accumulator * element);

        System.out.println(integer);
        System.out.println(integer1);
    }
}
