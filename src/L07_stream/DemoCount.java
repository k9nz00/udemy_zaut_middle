package L07_stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DemoCount {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("HellO");
        list.add("Bye");
        list.add("Number");
        list.add("Data");
        list.add("Java");

        System.out.println(list);

        // Это терминальный метод
        long count = list.stream().count();
        System.out.println(count);


    }
}
