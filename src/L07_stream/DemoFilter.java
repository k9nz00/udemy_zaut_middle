package L07_stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DemoFilter {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("HellO");
        list.add("Bye");
        list.add("Number");
        list.add("Data");
        list.add("Java");

        System.out.println(list);

        List<Integer> collect = list.stream()
                .map(elem -> elem.length())
                .filter(elem -> elem > 4)
                .collect(Collectors.toList());

        System.out.println(collect);

    }
}
