package L07_stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DemoSorted {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("HellO");
        list.add("Bye");
        list.add("Number");
        list.add("Data");
        list.add("Java");

        System.out.println(list);

        List<String> collect = list.stream()
                .sorted()
                .collect(Collectors.toList());
        System.out.println(collect);

        List<Student> students = new ArrayList<>();

        Student st1 = new Student("Andrey", 20, 'm', 3, 9.1);
        Student st2 = new Student("Natasha", 21, 'f', 2, 10.1);
        Student st3 = new Student("Ivan", 25, 'm', 5, 9.5);
        Student st4 = new Student("Vasya", 28, 'm', 1, 7.1);
        Student st5 = new Student("Masha", 30, 'f', 1, 5.1);

        students.add(st1);
        students.add(st2);
        students.add(st3);
        students.add(st4);
        students.add(st5);

        students.stream()
                .sorted(Comparator.comparingDouble(Student::getAvgGrade))
                .forEach(System.out::println);
    }
}
