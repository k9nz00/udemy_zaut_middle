package L07_stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DemoMap {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("HellO");
        list.add("Bye");
        list.add("Number");
        list.add("Data");
        list.add("Java");

        System.out.println(list);

        List<Integer> collect = list.stream()
                .map(elem -> elem.length())
                .collect(Collectors.toList());

        System.out.println(collect);

    }
}
