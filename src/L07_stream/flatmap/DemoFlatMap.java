package L07_stream.flatmap;

import java.util.ArrayList;
import java.util.List;

public class DemoFlatMap {
    public static void main(String[] args) {

        //сущности конечных объектов
        Student st1 = new Student("Andrey", 20, 'm', 3, 9.1);
        Student st2 = new Student("Natasha", 21, 'f', 2, 10.1);
        Student st3 = new Student("Ivan", 25, 'm', 5, 9.5);
        Student st4 = new Student("Vasya", 28, 'm', 1, 7.1);
        Student st5 = new Student("Masha", 30, 'f', 1, 5.1);

        //студенты находятся на разных факультетах
        Faculty f1 = new Faculty("Математика");
        Faculty f2 = new Faculty("Химия");

        f1.addStudentToFaculty(st1);
        f1.addStudentToFaculty(st2);
        f1.addStudentToFaculty(st3);

        f2.addStudentToFaculty(st4);
        f2.addStudentToFaculty(st5);

        //необходимо поработать с каждым стендом, независимо от того в каком он факультете
        List<Faculty> facultyList = new ArrayList<>();
        facultyList.add(f1);
        facultyList.add(f2);

        //flatMap позволяет работать со списком, который так же является полем в другом списке (
            // список факульететов
            // -> спикок студентов в каждом факултете
            // -> обходим каждого студента, независимо от того в каком факультете он находится)
        facultyList.stream()
                .flatMap(faculty -> faculty.getStudentsOnFaculty().stream()) //не забыть создать stream
                .forEach(student -> System.out.println(student.getName()));
    }
}
