package L07_stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DemoDistinct {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("HellO");
        list.add("HellO");
        list.add("Bye");
        list.add("Bye");
        list.add("Number");
        list.add("Number");
        list.add("Data");
        list.add("Java");
        list.add("Java");

        System.out.println(list);

        list = list.stream()
                .sorted()
                .distinct()
                .collect(Collectors.toList());
        System.out.println(list);

        List<Student> students = new ArrayList<>();

        Student st1 = new Student("Andrey", 20, 'm', 3, 9.1);
        Student st6 = new Student("Andrey", 20, 'm', 3, 9.1);
        Student st2 = new Student("Natasha", 21, 'f', 2, 10.1);
        Student st3 = new Student("Ivan", 25, 'm', 5, 9.5);
        Student st4 = new Student("Vasya", 28, 'm', 1, 7.1);
        Student st5 = new Student("Masha", 30, 'f', 1, 5.1);
        Student st7 = new Student("Masha", 30, 'f', 1, 5.1);

        students.add(st1);
        students.add(st2);
        students.add(st3);
        students.add(st4);
        students.add(st5);
        students.add(st6);
        students.add(st7);

        /*
        Необходимо переписать методы hashCode и equals, так как уникальность опеределяется при помощи метода equals  из класса Object
         */
        students.stream()
                .distinct()
                .sorted(Comparator.comparingDouble(Student::getAvgGrade))
                .forEach(System.out::println);
    }
}
