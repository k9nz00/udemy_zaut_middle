package L07_stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DemoForEach {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("HellO");
        list.add("Bye");
        list.add("Number");
        list.add("Data");
        list.add("Java");

        System.out.println(list);
        System.out.println("---------------");

        list.stream()
                .forEach(System.out::println);

    }
}
