package L06_lambda.consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class SupplierDemo {

    public static void main(String[] args) {

        Car originCar = new Car("Lada", "Granta", 1.6);

        System.out.println("origin car" + originCar);
        changeCar(originCar, chancedCar -> {
            originCar.setModel("Mazda");
            originCar.setColor("braun");
            originCar.setEngine(10.0);
        } );
        System.out.println("chance car" + originCar);


    }



    public static void changeCar(Car car, Consumer<Car> carConsumer){
        carConsumer.accept(car);
    }
}
