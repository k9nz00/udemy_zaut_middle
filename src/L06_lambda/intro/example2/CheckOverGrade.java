package L06_lambda.intro.example2;

public class CheckOverGrade implements StudentChecks {

    @Override
    public boolean check(Student student) {
        return student.avgGrade > 8;
    }
}
