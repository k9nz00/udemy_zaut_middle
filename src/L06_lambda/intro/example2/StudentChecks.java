package L06_lambda.intro.example2;

@FunctionalInterface
public interface StudentChecks {
    public boolean check(Student student);
}
