package L06_lambda.intro.example2;

import java.util.ArrayList;
import java.util.List;

public class StudentsInfo {

    public void studentCheck(List<Student> students, StudentChecks studentChecks) {
        for (Student student : students) {
            if (studentChecks.check(student)) {
                System.out.println(student);
            }
        }
    }

    public static void main(String[] args) {


        Student st1 = new Student("Andrey", 20, 'm', 3, 9.1);
        Student st2 = new Student("Natasha", 21, 'f', 2, 10.1);
        Student st3 = new Student("Ivan", 25, 'm', 5, 9.5);
        Student st4 = new Student("Vasya", 28, 'm', 1, 7.1);
        Student st5 = new Student("Masha", 30, 'f', 1, 5.1);

        List<Student> students = new ArrayList<>();
        students.add(st1);
        students.add(st2);
        students.add(st3);
        students.add(st4);
        students.add(st5);

        StudentsInfo studentsInfo = new StudentsInfo();


        //1
        studentsInfo.studentCheck(students, new CheckOverGrade());

        //2
        studentsInfo.studentCheck(students, new StudentChecks() {
            @Override
            public boolean check(Student student) {
                return student.avgGrade > 8;
            }
        });

        //3
        studentsInfo.studentCheck(students, (Student student) -> {
            return student.avgGrade > 8;
        });

        //4
        studentsInfo.studentCheck(students, (Student student) -> student.avgGrade > 8);

        //5
        studentsInfo.studentCheck(students, student -> student.avgGrade > 8);
    }
    /*
    Варианты 1 - 5  эквивалентны между собой
     */
}
