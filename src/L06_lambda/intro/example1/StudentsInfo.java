package L06_lambda.intro.example1;

import java.util.ArrayList;
import java.util.List;

public class StudentsInfo {
    public static void main(String[] args) {

        Student st1 = new Student("Andrey", 20, 'm', 3, 9.1);
        Student st2 = new Student("Natasha", 21, 'f', 2, 10.1);
        Student st3 = new Student("Ivan", 25, 'm', 5, 9.5);
        Student st4 = new Student("Vasya", 28, 'm', 1, 7.1);
        Student st5 = new Student("Masha", 30, 'f', 1, 5.1);

        List<Student> students = new ArrayList<>();
        students.add(st1);
        students.add(st2);
        students.add(st3);
        students.add(st4);
        students.add(st5);

        StudentsInfo studentsInfo = new StudentsInfo();

        studentsInfo.printStudentsOverGrade(students, 9.0);
        System.out.println("----------------------------------");
        studentsInfo.printStudentsUnderAge(students, 21);
        System.out.println("----------------------------------");
        studentsInfo.printStudentsMixCondition(students, 20, 7.5, 'f');
    }

    public void printStudentsOverGrade(List<Student> students, double grade) {
        for (Student student : students) {
            if (student.avgGrade > grade) {
                System.out.println(student);
            }
        }
    }

    public void printStudentsUnderAge(List<Student> students, int age) {
        for (Student student : students) {
            if (student.age < age) {
                System.out.println(student);
            }
        }
    }

    public void printStudentsMixCondition(List<Student> students, int age, double grade, char sex) {
        for (Student student : students) {
            if (student.age > age && student.avgGrade < grade && student.sex == sex) {
                System.out.println(student);
            }
        }
    }
}
