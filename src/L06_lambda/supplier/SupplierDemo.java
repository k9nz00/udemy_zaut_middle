package L06_lambda.supplier;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class SupplierDemo {

    public static void main(String[] args) {
        List<Car> cars = createThreeCars(()-> new Car("BMW", "red", 2.5));
        System.out.println(cars);
    }

    public static List<Car> createThreeCars(Supplier<Car> carSupplier) {

        List<Car> cars = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            cars.add(carSupplier.get());
        }
        return cars;
    }

}
